# hb #

`hb` is an HTTP benchmarking tool based on [libev](http://libev.schmorp.de).

# Developing #

Initialize autotools:

```bash
autoreconf -iv
./configure
make
```