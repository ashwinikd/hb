#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>
#include "../../config.h"
#include "config.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include "../lib/request.h"
#include "../lib/client.h"
#include "../lib/worker.h"
#include <unistd.h>

#if HAVE_GNUTLS
#include <gnutls/gnutls.h>
#endif

/**
 * Long options for the program
 */
static struct option _long_options[] = {
    {"version",      no_argument,       NULL, 'V'},  // print version
    {"help",         no_argument,       NULL, 'h'},  // print usage
    {"requests",     required_argument, NULL, 'n'},  // number of requests
    {"concurrent",   required_argument, NULL, 'c'},  // number of concurrent 
                                                     //   requests
#if HAVE_LIBPTHREAD
    {"threads",      required_argument, NULL, 't'},  // number of threads
#endif
    {"keep-alive",   no_argument,       NULL, 'k'},  // enable http keep alive
    {"use-http-1-0", no_argument,       NULL, '1'},  // use HTTP/1.0
    {"user-agent",   required_argument, NULL, 'u'},  // override user-agent
    {"header",       required_argument, NULL, 'h'},  // add custom http header
    {"verify-cert",  no_argument,       NULL, 'E'}   // verify server's certificate
};

/**
 * see https://tools.ietf.org/html/rfc7230#section-3.2
 */
static int
validate_header(char* header)
{
    hbreqheader* h = hbreqheader_new(header);
    int r = 0;
    if (h)
    {
        r = 1;
        hbreqheader_free(h);
    }
    return r;
}

static
int
_parse_uri(char* u, hbconfig* config)
{
    if(strlen(u) < 8) 
    {
        fprintf(stderr, "URL too short: %s\n", u);
        return 0;
    }
    char* uri = strdup(u);
    char* tmp = uri;
    if (! strncasecmp(uri, "http://", 7))
    {
        uri = uri + 7;
#if HAVE_GNUTLS
        config->secure = 0;
    }
    else if (! strncasecmp(uri, "https://", 8))
    {
        uri = uri + 8;
        config->secure = 1;
    }
#else
    }
#endif
    else 
    {
        fprintf(stderr, "Scheme not supported: %s\n", uri);
        free(tmp);
        return 0;
    }

    if(config->path) free(config->path);
    char* path = strchr(uri, '/');
    if(path) 
    {
        config->path = strdup(path);
        uri[path - uri] = '\0';
    } 
    else 
    {
        config->path = strdup("/");
    }

    char *port = strchr(uri, ':');
    if(port) 
    {
        uri[port - uri] = '\0';
        port = port + 1;
        config->port = atoi(port);
        if(! config->port) {
            fprintf(stderr, "Invalid port: %s\n", port);
            free(tmp);
            return 0;
        }
    } 
    else 
    {
#if HAVE_GNUTLS
        config->port = config->secure ? 443 : 80;
#else
        config->port = 80;
#endif
    }

    if(config->host) free(config->host);
    config->host = strdup(uri);
    free(tmp);
    return 1;
}

static int
_resolve_host_v4(struct addrinfo **addr, const char *host)
{
    struct addrinfo hints, *res, *res0, *res_last;
    int error;

    memset(&hints, 0, sizeof(hints));
    hints.ai_family = PF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;

    error = getaddrinfo(host, NULL, &hints, &res0);
    if(error)
    {
        return error;
    }

    res_last = 0;
    for(res = res0; res; res = res->ai_next)
    {
        if(res->ai_family == AF_INET) break;
        res_last = res;
    }

    if(! res)
    {
        freeaddrinfo(res0);
        return 1;
    }

    if( res != res0 )
    {
        res_last->ai_next = res->ai_next;
        freeaddrinfo(res0);
        res->ai_next = 0;
    }

    *addr = res;
    return 0;
}

/**
 * Print version information
 */
static void
_print_version()
{
    printf("%s - %s\n", PACKAGE_NAME, PACKAGE_VERSION);
}

/**
 * Print help and usage
 */
static void
_print_help()
{
    printf("Usage: "
        "./src/bin/hb -n <num_requests> [Options ...] <url>"
    );
    printf("\n"
        "Options:\n"
        "  -V, --version        Print version information\n"
        "  -h, --help           Print this help\n"
        "  -n, --requests       (required) Total number of requests to make.\n"
        "  -c, --concurrent     Number of concurrent requests to make. By \n"
        "                       default this is 1.\n"
#if HAVE_LIBPTHREAD
        "  -t, --threads        Number of trheads to use. Selects appropriate\n"
        "                       value based on number of CPUs and concurrency.\n"
#endif
        "  -k, --keep-alive     Enable HTTP connection keep-alive. By default\n"
        "                       keep-alive is disabled.\n"
        "  -1, --use-http-1-0   Use HTTP/1.0. Default is to use HTTP/1.1\n"
        "  -u, --user-agent     Override user agent string. Default is hb/0.0.1.\n"
        "  -H, --header         Add custom HTTP header. Can be used multiple \n"
        "                       times.\n"
        "  -E, --verify-cert    Verify the server certificate when working \n"
        "                       with HTTPS. By default this is OFF.\n"
    );
}

#if HAVE_GNUTLS

static int 
_verify_cert_cb(gnutls_session_t session)
{
    unsigned int status;
    const gnutls_datum_t *cert_list;
    unsigned int cert_list_size;
    int ret;
    gnutls_x509_crt_t cert;
    const char* hostname;

    hostname = gnutls_session_get_ptr(session);
    ret = gnutls_certificate_verify_peers2(session, &status);

    if (ret < 0)
    {
#if DEBUG
        fprintf(stderr, "Error in verifying certificate.\n");
#endif
        return GNUTLS_E_CERTIFICATE_ERROR;
    }

#if DEBUG
    if (status & GNUTLS_CERT_SIGNER_NOT_FOUND)
        fprintf(stderr, "The certificate hasn't got a known issuer.\n");

    if (status & GNUTLS_CERT_REVOKED)
        fprintf(stderr, "The certificate has been revoked.\n");

    if (status & GNUTLS_CERT_EXPIRED)
        fprintf(stderr, "The certificate has expired\n");

    if (status & GNUTLS_CERT_NOT_ACTIVATED)
        fprintf(stderr, "The certificate is not yet activated\n");
#endif

    if (status & GNUTLS_CERT_INVALID) // cert not trusted
    {
#if DEBUG
        fprintf(stderr, "The certificate is not trusted.\n");
#endif
        return GNUTLS_E_CERTIFICATE_ERROR;
    }

    if (gnutls_certificate_type_get(session) != GNUTLS_CRT_X509)
    {
#if DEBUG
        fprintf(stderr, "Certificate is not X509.\n");
#endif
        return GNUTLS_E_CERTIFICATE_ERROR;
    }

    if (gnutls_x509_crt_init(&cert) < 0)
    {
#if DEBUG
        fprintf(stderr, "Error in initializing certificate.\n");
#endif
        return GNUTLS_E_CERTIFICATE_ERROR;
    }

    cert_list = gnutls_certificate_get_peers(session, &cert_list_size);
    if (cert_list == NULL) // no certificates
    {
#if DEBUG
        fprintf(stderr, "No certificates were found.\n");
#endif
        gnutls_x509_crt_deinit(cert);
        return GNUTLS_E_CERTIFICATE_ERROR;
    }

    if (gnutls_x509_crt_import(cert, &cert_list[0], GNUTLS_X509_FMT_DER) < 0)
    {
#if DEBUG
        fprintf(stderr, "Error in parsing certificate.\n");
#endif
        gnutls_x509_crt_deinit(cert);
        return GNUTLS_E_CERTIFICATE_ERROR;
    }

    if (! gnutls_x509_crt_check_hostname(cert, hostname))
    {
#if DEBUG
        printf ("The certificate's owner does not match hostname '%s'\n",
                hostname);
#endif
        gnutls_x509_crt_deinit(cert);
        return GNUTLS_E_CERTIFICATE_ERROR;
    }

    gnutls_x509_crt_deinit(cert);
    return 0;
}

#endif

static void
_on_req_complete(hbclient* client, void* cb_data)
{
    hbconfig* config = (hbconfig*) cb_data;
    static int num_executed = 0;

    if (client->state != READY)
    {
        printf("Client is not ready. State=%d\n", client->state);
        if (client->request)
            hbrequest_free(client->request);
        printf("Hello\n");
        if (client->state != CLOSED)
            hbclient_close(client, 1);
        return;
    }
    if (num_executed >= config->n_requests)
    {
        printf("Executed %d requests\n", num_executed);
        if (client->request)
            hbrequest_free(client->request);
        if (client->state != CLOSED)
            hbclient_close(client, 1);
        return;
    }

    printf("Ready to execute request.\n");
    hbrequest* request = hbrequest_new();
    if (request->path)
    {
        free(request->path);
        request->path = NULL;
    }
    request->path = strdup(config->path);
    if (config->http_1_0) 
        request->version = HTTP_VERSION_1_0;
    else
    {
        // Add host header
        char* host_header = malloc((6 + strlen(config->host) + 1) * sizeof(char));
        if (! host_header)
        {
            fprintf(stderr, "Error in creating host header.\n");
            hbrequest_free(request);
            return;
        }
        sprintf(host_header, "Host: %s", config->host);
        if (! hbrequest_addhead(request, host_header))  
        {
            fprintf(stderr, "Error in adding host header.\n");
            free(host_header);
            hbrequest_free(request);
            return;
        }
        free(host_header);

        if (! config->keep_alive 
            && ! hbrequest_addhead(request, "Connection: close"))
        {
            fprintf(stderr, "Error in adding connection header.\n");
            hbrequest_free(request);
            return;
        }
    }

    // add user agent header
    char* ua_header = malloc(
                (12 + strlen(config->user_agent) + 1) * sizeof(char));
    if (! ua_header)
    {
        fprintf(stderr, "Error in building ua header\n");
        hbrequest_free(request);
        return;
    }
    sprintf(ua_header, "User-Agent: %s", config->user_agent);
    if (! hbrequest_addhead(request, ua_header))  
    {
        fprintf(stderr, "Error in adding ua header.\n");
        free(ua_header);
        hbrequest_free(request);
        return;
    }
    free(ua_header);

    // add custom headers
    for (int i = 0; i < config->n_custom_headers; i++)
    {
        if (! hbrequest_addhead(request, config->custom_headers[i]))
        {
            fprintf(stderr, "Error in adding headers\n");
            hbrequest_free(request);
            return;
        }
    }

    hbrequest_print(request);
    int err = hbclient_execute(client, request, _on_req_complete,
                               _on_req_complete, config);
    if (err)
    {
        fprintf(stderr, "Error in executing request: %d\n", err);
        fprintf(stderr, "Current state = %d\n", client->state);
        hbrequest_free(request);
        hbclient_close(client, 1);
    }
    num_executed++;
}

static void
_on_connect(hbclient* client, void* cb_data)
{
    if (client->state != READY)
    {
        fprintf(stderr, "Error in connecting: %s\n", client->buffer);
        if (client->request)
            hbrequest_free(client->request);
        if (client->state != CLOSED)
            hbclient_close(client, 1);
        return;
    }
    _on_req_complete(client, cb_data);
}

static void
_test(hbconfig* config)
{
    struct ev_loop* loop = ev_loop_new(EVLOOP_FLAG);
    hbclient* client = hbclient_new(loop, config->addr);
    if (! client) 
    {
        printf("Unable to create client\n");
        return;
    }
#if HAVE_GNUTLS
    client->secure = config->secure;
    if (client->secure)
    {
        client->ssl_host = config->host;
        client->tls_priority_cache = &config->tls_priority_cache;
        client->ssl_cred = &config->ssl_cred;
    }
#endif
    client->keep_alive = config->keep_alive;
    int err = hbclient_connect(client, _on_connect, config);
    if (err)
    {
        fprintf(stderr, "Error in connecting: %d: %s\n", err, client->buffer);
    }
    ev_run(loop, 0);
    hbclient_free(client);
}

int
main(int argc, char** argv)
{
    char opt;
    int long_index = 0;

    hbconfig* config = hbconfig_create();
    config->n_custom_headers = 0;
    config->custom_headers = malloc(sizeof(char*));

    while( (opt = getopt_long(argc, argv, "Vhn:c:t:k1u:H:E", 
            _long_options, &long_index)) != -1 )
    {
        switch(opt)
        {
            case 'V':
                _print_version();
                return EXIT_SUCCESS;
                break;
            case 'h':
                _print_help();
                return EXIT_SUCCESS;
                break;
            case 'n':
                config->n_requests = atoi(optarg);
                break;
            case 'c':
                config->n_concurrent = atoi(optarg);
                break;
#if HAVE_LIBPTHREAD
            case 't':
                config->n_threads = atoi(optarg);
                break;
#endif
            case 'k':
                config->keep_alive = 1;
                break;
            case '1':
                config->http_1_0 = 1;
                break;
            case 'u':
                if (config->user_agent)
                {
                    free(config->user_agent);
                    config->user_agent = NULL;
                }
                config->user_agent = malloc(strlen(optarg) * sizeof(char) + 1);
                strcpy(config->user_agent, optarg);
                break;
            case 'H':
                if (! validate_header(optarg))
                {
                    fprintf(stderr, "Header string [ %s ] is not valid.\n", 
                        optarg);
                    hbconfig_free(config);
                    return EXIT_FAILURE;
                }

                int new_header_count = config->n_custom_headers + 1;
                char** new_headers = realloc(config->custom_headers,
                                new_header_count * sizeof(char*));
                if (! new_headers)
                {
                    fprintf(stderr, "Too many custom headers.\n");
                    hbconfig_free(config);
                    return EXIT_FAILURE;
                }
                config->custom_headers = new_headers;
                config->custom_headers[config->n_custom_headers] 
                    = malloc(strlen(optarg) * sizeof(char) + 1);
                strcpy(config->custom_headers[config->n_custom_headers],
                        optarg);
                config->n_custom_headers = new_header_count;
                break;
            case 'E':
                config->verify_cert = 1;
                break;
            default:
                _print_help();
                hbconfig_free(config);
                return EXIT_FAILURE;
        }
    }

    if (optind == argc) {
        fprintf(stderr, "Too few arguments. Target url is required.\n");
        _print_help();
        hbconfig_free(config);
        return EXIT_FAILURE;
    }

    if (optind + 1 < argc) {
        fprintf(stderr, "Too many arguments.\n");
        _print_help();
        hbconfig_free(config);
        return EXIT_FAILURE;
    }

    if (! _parse_uri(argv[optind], config))
    {
        hbconfig_free(config);
        return EXIT_FAILURE;
    }

    if (config->addr) free(config->addr);
    config->addr = malloc(sizeof(struct addrinfo));
    if (_resolve_host_v4(&config->addr, config->host))
    {
        fprintf(stderr, "Error in resolving host %s.\n", config->host);
        hbconfig_free(config);
        return EXIT_FAILURE;
    }
    struct sockaddr_in *addr = (struct sockaddr_in *) config->addr->ai_addr;
    addr->sin_port = htons(config->port);

#if HAVE_LIBPTHREAD
    int num_cpu = (int) sysconf(_SC_NPROCESSORS_ONLN);
    if (! config->n_threads)
    {
        config->n_threads = num_cpu > config->n_concurrent 
                    ? config->n_concurrent : num_cpu;
    }
#endif

    if (! hbconfig_validate(config))
    {
        fprintf(stderr, "ERROR: %s\n\n", hbconfig_err_msg);
        _print_help();
        printf("\nProvided configuration: \n");
        hbconfig_print(config);
        hbconfig_free(config);
        return EXIT_FAILURE;
    }

    hbconfig_print(config);

#if HAVE_GNUTLS
    if (config->secure)
    {
        gnutls_global_init();
        gnutls_certificate_allocate_credentials(&config->ssl_cred);
        int r = gnutls_certificate_set_x509_system_trust(config->ssl_cred);
        if (config->verify_cert && r == GNUTLS_E_UNIMPLEMENTED_FEATURE)
        {
            fprintf(stderr, "==========================================\n"
                            "Loading CA certificates from system is not\n"
                            "supported on this platform. Will be using\n"
                            "certificate lists shipped with this program.\n"
                            "==========================================\n");
            r = gnutls_certificate_set_x509_trust_file(config->ssl_cred, CAFILE, 
                GNUTLS_X509_FMT_PEM);
            if (r < 1)
            {
                fprintf(stderr, "Error in loading CA certificates. Certificate "
                                "verification\nwill be disabled.");  
                config->verify_cert = 0;
            }
        }
        if (config->verify_cert) {
            gnutls_certificate_set_verify_function(config->ssl_cred, 
                _verify_cert_cb);
        }

        int ret = gnutls_priority_init(&config->tls_priority_cache, 
            config->ssl_priority, 0);
        if (ret) {
            fprintf(stderr, "Invalid priority string for SSL: %s\n\n", 
                config->ssl_priority);
            gnutls_certificate_free_credentials(config->ssl_cred);
            gnutls_global_deinit();
            hbconfig_free(config);
            return EXIT_FAILURE;
        }
    }
#endif
        
    _test(config);

#if HAVE_GNUTLS
    if (config->secure)
    {
        gnutls_priority_deinit(config->tls_priority_cache);
        gnutls_certificate_free_credentials(config->ssl_cred);
        gnutls_global_deinit();
    }
#endif

    hbconfig_free(config);
    return EXIT_SUCCESS;
}
