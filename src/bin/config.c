#include "config.h"

char* hbconfig_err_msg = "";

hbconfig*
hbconfig_create()
{
    hbconfig* config = malloc(sizeof(hbconfig));
    config->n_requests = 0;
    config->n_concurrent = 1;
#if HAVE_LIBPTHREAD
    config->n_threads = 0;
#endif
    config->keep_alive = 0;
    config->http_1_0 = 0;
    config->user_agent = malloc(strlen(PACKAGE_NAME) 
                                + strlen(PACKAGE_VERSION) 
                                + 2);
    sprintf(config->user_agent, "%s/%s", PACKAGE_NAME, PACKAGE_VERSION);
    config->n_custom_headers = 0;
    config->custom_headers = NULL;
    config->host = NULL;
    config->port = -1;
    config->path = NULL;
    config->addr = NULL;
#if HAVE_GNUTLS
    config->secure = 0;
    config->ssl_priority = "NORMAL";
    config->verify_cert = 0;
#endif
    return config;
}

void
hbconfig_free(hbconfig* config)
{
    if (config->addr)
    {
        free(config->addr);
        config->addr = NULL;
    }

    if (config->user_agent)
    {
        free(config->user_agent);
        config->user_agent = NULL;
    }

    if (config->host)
    {
        free(config->host);
        config->host = NULL;
    }

    if (config->path)
    {
        free(config->path);
        config->path = NULL;
    }

    for (int i = 0; i < config->n_custom_headers; i++)
    {
        if (config->custom_headers[i])
        {
            free(config->custom_headers[i]);
            config->custom_headers[i] = NULL;
        }
    }
    if (config->custom_headers)
    {
        free(config->custom_headers);
        config->custom_headers = NULL;
    }

    if (config->addr)
    {
        free(config->addr);
        config->addr = NULL;
    }

    free(config);
}

int
hbconfig_validate(hbconfig* config)
{
    if (config->n_requests < 1)
    {
        hbconfig_err_msg = "Number of total requests should be positive.";
        return 0;
    }

    if (config->n_concurrent < 1)
    {
        hbconfig_err_msg = "Number of concurrent requests should be positive.";
        return 0;
    }

    if (config->n_requests < config->n_concurrent)
    {
        hbconfig_err_msg = "Number of concurrent requests should be less than "
                           "total number of requests.";
        return 0;
    }

#if HAVE_LIBPTHREAD
    if (config->n_threads < 1)
    {
        hbconfig_err_msg = "Number of threads should be positive.";
        return 0;
    }

    if (config->n_requests < config->n_threads)
    {
        hbconfig_err_msg = "Number of threads should be less than total "
                           "number of requests.";
        return 0;
    }

    if (config->n_concurrent < config->n_threads)
    {
        hbconfig_err_msg = "Number of threads should be less than number of "
                           "concurrent requests.";
        return 0;
    }
#endif

    if (config->keep_alive && config->http_1_0)
    {
        hbconfig_err_msg = "Cannot use connection keep alive with HTTP/1.0.";
        return 0;
    }

    return 1;
}

void
hbconfig_print(hbconfig* config)
{
    char headers[1024];
    headers[0] = '\0';
    int wpos = 0;
    for(int i = 0; i < config->n_custom_headers; i++) {
        wpos += sprintf(headers + wpos, "    %s\n", config->custom_headers[i]);
    }

    printf("hbconfig {\n"
        "  n_requests: %d\n"
        "  n_concurrent: %d\n"
#if HAVE_LIBPTHREAD
        "  n_threads: %d\n"
#endif
        "  keep_alive: %s\n"
        "  http_version: %s\n"
        "  user_agent: %s\n"
        "  custom_headers: [\n"
        "%s"
        "  ]\n"
        "  host: %s\n"
        "  port: %d\n"
        "  path: %s\n"
#if HAVE_GNUTLS
        "  secure: %s\n"
#endif
        "}\n",
        config->n_requests,
        config->n_concurrent,
#if HAVE_LIBPTHREAD
        config->n_threads,
#endif
        config->keep_alive ? "yes" : "no",
        config->http_1_0 ? "HTTP/1.0" : "HTTP/1.1",
        config->user_agent,
        headers,
        config->host,
        config->port,
#if HAVE_GNUTLS
        config->path,
        config->secure ? "yes" : "no"
#else
        config->path
#endif
    );
}
