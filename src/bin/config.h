#ifndef _CONFIG_H_
#define _CONFIG_H_

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include "../../config.h"

#if HAVE_GNUTLS
#include <gnutls/gnutls.h>
#include <gnutls/x509.h>
#endif

extern char* hbconfig_err_msg;

typedef struct hbconfig
{
    // required config
    int n_requests;             // number of requests to make
    int n_concurrent;           // number of concurrent connections
#if HAVE_LIBPTHREAD
    int n_threads;              // number of threads to use
#endif

    // HTTP headers
    int keep_alive;             // use HTTP connection keep alive
    int http_1_0;               // use HTTP/1.0
    char* user_agent;           // custom user agent
    int n_custom_headers;       // number of custom headers
    char** custom_headers;      // custom headers list

    // Input URI
    char* host;                 // host
    int port;                   // port
    char* path;                 // path

    // connection information
    struct addrinfo* addr;      // socket address of the server
#if HAVE_GNUTLS
    int secure;                 // use https
    gnutls_priority_t tls_priority_cache;
    gnutls_certificate_credentials_t ssl_cred;
    char* ssl_priority;
    int verify_cert;
#endif
} hbconfig;

hbconfig* hbconfig_create();
void hbconfig_free(hbconfig* config);
void hbconfig_print(hbconfig* config);
int hbconfig_validate(hbconfig* config);

#endif
