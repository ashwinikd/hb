#include "client.h"

static int _setupsock(int fd);
static void _on_connect(struct ev_loop* loop, ev_io* w, int revents);
static void _on_read(struct ev_loop* loop, ev_io* w, int revents);
static void _on_write(struct ev_loop* loop, ev_io* w, int revents);
static int _find_eoh(char* str, int len);
static int _parse_headers(hbclient* client, int eoh);
static int _find_head_value(char* str, char* q);
static void _rearm_client(hbclient* client);
static int _read_chunk_len(hbcdstate* state, char* buf, int n);
static ssize_t _conn_read(hbclient* client, void* buffer, size_t size);
static ssize_t _conn_write(hbclient* client, void* buffer, size_t size);


#if HAVE_GNUTLS
static void _ssl_handshake(hbclient* client);
static ssize_t _ssl_push(gnutls_transport_ptr_t f, const void* b, size_t n);
static ssize_t _ssl_pull(gnutls_transport_ptr_t f, void* b, size_t n);
#endif

void 
hbcdstate_init(hbcdstate* state)
{
    state->state = HBCD_STOPPED;
    state->n_chunk = 0;
    state->chunk_size = 0;
    state->chunk_rpos = 0;
    state->cr_found = 0;
    state->crlf_cnt = 0;
    state->len = 0;
}

void 
hbcd_decode(hbcdstate* state, char* buf, int len)
{
    int bytes_read = 0;
    if (state->state == HBCD_STOPPED) state->state = HBCD_IDLE;
    while (bytes_read < len && state->state != HBCD_FINISHED)
    {
        if (state->state == HBCD_IDLE)
        {
            state->len = 0;
            state->chunk_rpos = 0;
            state->state = HBCD_READING_LEN;
            bytes_read += _read_chunk_len(state, buf + bytes_read, 
                len - bytes_read);
#if DEBUG
        printf("Reading Chunk#%d of size %d\n", state->n_chunk, 
            state->chunk_size);
#endif
        }
        else if (state->state == HBCD_READING_LEN)
        {
            bytes_read += _read_chunk_len(state, buf + bytes_read, 
                len - bytes_read);
#if DEBUG
        printf("Reading Chunk#%d of size %d\n", state->n_chunk, 
            state->chunk_size);
#endif
        }
        else if (state->state == HBCD_READING_BODY)
        {
            if (state->chunk_size == 0)
            {
                state->chunk_rpos = 0;
                state->len = 0;
                state->cr_found = 0;
                state->crlf_cnt = 0;
                state->state = HBCD_READING_TRLR;
#if DEBUG
        printf("Reading Trailers\n");
#endif
                continue;
            }
            if (state->chunk_rpos == state->chunk_size + 2)
            {
                state->state = HBCD_IDLE;
                continue;    
            }

            // ignore the chunk body
            state->chunk_rpos++;
            bytes_read++;
        }
        else if (state->state == HBCD_READING_TRLR)
        {
            char c = buf[bytes_read];
            bytes_read++;
            state->chunk_rpos++;
            if (state->cr_found)
            {
                if (c == '\n')
                {
                    if (state->chunk_rpos == 2) // empty trailer
                        state->state = HBCD_FINISHED;
                    else
                    {
                        state->cr_found = 0;
                        state->crlf_cnt++;
                        if (state->crlf_cnt == 2)
                            state->state = HBCD_FINISHED;
                    }
                }
            }
            if (c == '\r')
                state->cr_found = 1;
            else
                state->cr_found = 0;
        }
    }
}

static int 
_read_chunk_len(hbcdstate* state, char* buf, int n)
{
    int cr_found = 0, i;
    char c;
    for(i = 0; i < n; i++)
    {
        c = buf[i];
        if (cr_found)
        {
            if (c == '\n')
            {
                if (state->state == HBCD_READING_LEN)
                {
                    state->buf[state->len] = '\0';
                    state->chunk_size = (int) strtol(state->buf, NULL, 16);
                    state->chunk_rpos = 0;
                    state->n_chunk++;
                    state->state = HBCD_READING_BODY;
                    state->len = 0;
                }
                break;
            }
        }
        if (c == '\r')
        {
            cr_found = 1;
            continue;
        }
        if (state->state == HBCD_READING_LEN)
        {
            if (c == ';')
            {
                state->buf[state->len] = '\0';
                state->chunk_size = (int) strtol(state->buf, NULL, 16);
                state->chunk_rpos = 0;
                state->n_chunk++;
                state->state = HBCD_READING_BODY;
                state->len = 0;
            }
            else
            {
                state->buf[state->len] = c;
                state->len++;
            }
        }
    }
    return i + 1;
}

hbclient* 
hbclient_new(struct ev_loop* loop, struct addrinfo* addr)
{
    hbclient* client = malloc(sizeof(hbclient));
    client->state = CLOSED;
    client->addr = addr;
#if HAVE_GNUTLS
    client->secure = 0;
    memset(&client->tls_session, 0, sizeof(client->tls_session));
#endif
    client->loop = loop;
    client->wpos = 0;
    client->rpos = 0;
    client->client_cb = NULL;
    client->success_cb = NULL;
    client->err_cb = NULL;
    client->chunked = 0;
    client->response_len = 0;
    client->request = NULL;
    memset(&client->cdstate, 0, sizeof(client->cdstate));
    ev_io_init(&client->conn_cb, _on_connect, -1, EV_WRITE);
    ev_io_init(&client->read_cb, _on_read, -1, EV_READ);
    ev_io_init(&client->write_cb, _on_write, -1, EV_WRITE);
    return client;
}

void
hbclient_free(hbclient* client)
{
    if (! client) return;
    free(client);
}

int
hbclient_connect(hbclient* client, hbclient_cb cb, void* cb_data)
{
    if (! client) return HBCLIENT_ERR_INVALID;
    if (client->state != CLOSED) return HBCLIENT_ERR_STATE;

    if (ev_is_active(&client->conn_cb))
        ev_io_stop(client->loop, &client->conn_cb);
    if (ev_is_active(&client->read_cb))
        ev_io_stop(client->loop, &client->read_cb);
    if (ev_is_active(&client->write_cb))
        ev_io_stop(client->loop, &client->write_cb);

    client->fd = socket(client->addr->ai_family, client->addr->ai_socktype,
        client->addr->ai_protocol);
    if(client->fd == -1) {
        strerror_r(errno, client->buffer, sizeof(client->buffer));
        return HBCLIENT_ERR_CONN;
    }
    client->state = CONNECTING;

    if(_setupsock(client->fd)) {
        sprintf(client->buffer, "Error in setting up non-blocking socket.\n");
        return HBCLIENT_ERR_CONN;
    }

    client->client_cb = cb;
    client->cb_data = cb_data;
    ev_io_set(&client->conn_cb, client->fd, EV_WRITE);
    ev_io_set(&client->read_cb, client->fd, EV_READ);
    ev_io_set(&client->write_cb, client->fd, EV_WRITE);
    ev_io_start(client->loop, &client->conn_cb);

    int err = connect(client->fd, client->addr->ai_addr, 
                        client->addr->ai_addrlen);

    if(err) {
        if(errno != EINPROGRESS && errno != EALREADY && errno != EISCONN) {
            strerror_r(errno, client->buffer, sizeof(client->buffer));
            client->state = CLOSED;
            if (ev_is_active(&client->conn_cb))
                ev_io_stop(client->loop, &client->conn_cb);
            client->client_cb = NULL;
            return HBCLIENT_ERR_CONN;
        }
    }

    return HBCLIENT_ERR_OK;
}

int
hbclient_execute(hbclient* client, hbrequest* request, hbclient_cb success_cb, 
                 hbclient_cb error_cb, void* data)
{
    if (client->state != READY) return HBCLIENT_ERR_STATE;

    if (ev_is_active(&client->conn_cb))
        ev_io_stop(client->loop, &client->conn_cb);
    if (ev_is_active(&client->read_cb))
        ev_io_stop(client->loop, &client->read_cb);
    if (ev_is_active(&client->write_cb))
        ev_io_stop(client->loop, &client->write_cb);

    client->state = WRITING;
    client->success_cb = success_cb;
    client->err_cb = error_cb;
    client->cb_data = data;
    client->request = request;
    client->chunked = 0;
    client->wpos = 0;
    client->rpos = 0;
    hbcdstate_init(&client->cdstate);
    client->response_keep_alive = request->version == HTTP_VERSION_1_1;
    client->response_len = 0;
    ev_io_start(client->loop, &client->write_cb);
    return HBCLIENT_ERR_OK;
}

void
hbclient_close(hbclient* client, int good)
{
    if (ev_is_active(&client->conn_cb))
        ev_io_stop(client->loop, &client->conn_cb);
    if (ev_is_active(&client->read_cb))
        ev_io_stop(client->loop, &client->read_cb);
    if (ev_is_active(&client->write_cb))
        ev_io_stop(client->loop, &client->write_cb);

    if (client->state == CLOSED) return;
#if DEBUG
        printf("Closing connection.\n");
#endif

#if HAVE_GNUTLS
    if (client->secure)
        gnutls_deinit(client->tls_session);
#endif

    if (good)
        close(client->fd);
    else
    {
        struct linger linger;
        linger.l_onoff=1;
        linger.l_linger=0; // timeout for completing writes
        setsockopt(client->fd, SOL_SOCKET, SO_LINGER, &linger, sizeof(linger));
        close(client->fd);
    }
    client->state = CLOSED;
}

static int
_setupsock(int fd)
{
    int flags = fcntl(fd, F_GETFL);
    if(flags < 0) return flags;
    if(fcntl(fd, F_SETFL, flags |= O_NONBLOCK) < 0) return -1;
    int nodelay = 1;
    int err = setsockopt(fd, IPPROTO_TCP, TCP_NODELAY,
        &nodelay, sizeof(nodelay));
    if(err) return -1;
    return 0;
}

static int 
_find_eoh(char* str, int len)
{
    int cr_found = 0;
    int crlf_cnt = 0;

    char c;
    for (int i = 0; i < len; i++)
    {
        c = str[i];
        if (cr_found)
        {
            if (c == '\n')
            {
                crlf_cnt += 1;
                if ( crlf_cnt == 2)
                    return i + 1;
                cr_found = 0;
                continue;
            }
        }
        
        if (c == '\r')
            cr_found = 1;
        else
        {
            cr_found = 0;
            crlf_cnt = 0;
        }
    }

    return 0;
}

static int
_parse_headers(hbclient* client, int eoh)
{
    char* hlines = malloc((eoh + 1) * sizeof(char));
    if (! hlines) return 0;
    strncpy(hlines, client->buffer, eoh);
    char* tmp = hlines;

#if DEBUG
    hlines[eoh - 1] = '\0';
    printf("%s\n", hlines);
#endif

    int cr_found = 0, crlf_found = 0;
    char c;

    // skip status line
    while (1)
    {
        c = *hlines;

        if(crlf_found) break;
        if (cr_found)
        {
            if (c == '\n')
            {
                crlf_found = 1;
            }
        }
        if (c == '\r')
            cr_found = 1;
        hlines++;
        eoh--;
    }

    cr_found = 0;
    crlf_found = 0;
    int s = 0;
    
    for (int i = 0; i < eoh; i++)
    {
        c = hlines[i];
        if (crlf_found)
        {
            if (c == ' ' || c == '\t') crlf_found = 0; // obs-fold multi-line
            else                                       // header
            {
                hlines[i - 2] = '\0';  // replace \r with \0
                hbreqheader* header = hbreqheader_new(hlines + s);
                if (! header)
                {
                    free(tmp);
                    return -2;
                }
                if (! strcasecmp(header->key, "connection"))
                {
                    if (_find_head_value(header->value, "keep-alive"))
                        client->response_keep_alive = 1;
                    else if (_find_head_value(header->value, "close"))
                        client->response_keep_alive = 0;
                    printf("Keep-Alive changed = %d\n", client->response_keep_alive);
                }
                if (! strcasecmp(header->key, "content-length"))
                {
                    client->response_len += atoi(header->value);
                }
                if (! strcasecmp(header->key, "transfer-encoding"))
                {
                    if (_find_head_value(header->value, "chunked"))
                        client->chunked = 1;
                }
                hbreqheader_free(header);
                s = i;
                crlf_found = 0;
                cr_found = 0;
            }
        }
        else if (cr_found)
        {
            if (c == '\n')
            {
                crlf_found = 1;
                cr_found = 0;
                continue;
            }
            else
            {
                free(tmp); 
                return -1; // CR without LF is illegal in headers
            }
        }
        else if (c == '\r')
            cr_found = 1;
    }

    free(tmp);
    return 1;
}

static int 
_find_head_value(char* str, char* q)
{
    int r = 0;
    int s = 0;
    int e = 0;
    int v = 0;
    int l = strlen(str);
    
    char* tmp = strdup(str);
    if (! tmp) return 0;

    char c;
    for (int i = 0; i < l; i++)
    {
        c = tmp[i];
        if (c == ',' || i == l - 1)
        {
            if (c != ',') e = i;
            tmp[e + 1] = '\0';
            if (! strcasecmp(tmp + s, q))
            {
                r = 1;
                break;
            }
            s = i + 1;
            v = 0;
        }
        else if (c != ' ' && c != '\t')
        {
            e = i;
            v = 1;
        }
        else if (! v)
        {
            s = i + 1;
        }
    }

    free(tmp);
    return r;
}

static void 
_rearm_client(hbclient* client)
{
    if (client->state == CLOSED) return;
    if (client->response_keep_alive && client->keep_alive)
    {
#if DEBUG
        printf("Keep-Alive is ON.\n");
#endif
        client->state = READY;
    }
    else
    {
#if DEBUG
        printf("Keep-Alive is Off. client=%d, server=%d\n", client->keep_alive,
            client->response_keep_alive);
#endif
        hbclient_close(client, 1);
        client->state = CLOSED;
    }
}

static ssize_t 
_conn_read(hbclient* client, void* buffer, size_t size)
{
#if HAVE_GNUTLS
    if (client->secure)
    {
        return gnutls_record_recv(client->tls_session, buffer, size);
    }
    else
#endif
    {
        return read(client->fd, buffer, size);
    }
}

static ssize_t 
_conn_write(hbclient* client, void* buffer, size_t size)
{
#if HAVE_GNUTLS
    if (client->secure)
    {
        return gnutls_record_send(client->tls_session, buffer, size);
    }
    else
#endif
    {
        return write(client->fd, buffer, size);
    }
}

#if HAVE_GNUTLS
static void 
_ssl_handshake(hbclient* client)
{
    int ret = gnutls_handshake(client->tls_session);
    if (ret == GNUTLS_E_SUCCESS)
    {
#if DEBUG
        printf("HANDSHAKE Done.\n");
#endif
        client->state = READY;
        if (client->client_cb)
            (*(client->client_cb))(client, client->cb_data);
    }
    else if (ret == GNUTLS_E_AGAIN || ! gnutls_error_is_fatal(ret))
    {
#if DEBUG
        printf("HANDSHAKE In progress.\n");
#endif
        if (gnutls_record_get_direction(client->tls_session))
            ev_io_start(client->loop, &client->write_cb);
        else
            ev_io_start(client->loop, &client->read_cb);
    }
    else
    {
        strcpy(client->buffer, gnutls_strerror(ret));
        client->buffer[1023] = '\0';
#if DEBUG
        printf("HANDSHAKE Error. %s\n", client->buffer);
#endif
        hbclient_close(client, 0);
        client->state = SSL_HANDSHAKE_FAILED;
        if (client->err_cb)
           (*client->err_cb)(client, client->cb_data);
    }
}

static ssize_t 
_ssl_push(gnutls_transport_ptr_t f, const void* b, size_t n)
{
    return write((int)(int_to_ptr)f, b, n);
}

static 
ssize_t _ssl_pull(gnutls_transport_ptr_t f, void* b, size_t n)
{
    return read((int)(int_to_ptr)f, b, n);
}

#endif

static void 
_on_connect(struct ev_loop* loop, ev_io* w, int revents)
{
    hbclient* client = (hbclient *) (((char *) w) -
        offsetof(hbclient, conn_cb));
    ev_io_stop(client->loop, &client->conn_cb);
    int err = connect(client->fd, client->addr->ai_addr, 
                        client->addr->ai_addrlen);
    if(err)
    {
        if(errno == EINPROGRESS || errno == EALREADY)
            ev_io_start(client->loop, &client->conn_cb);
        else if (errno == EISCONN)
        {
#if DEBUG
            printf("Connected\n");
#endif
#if HAVE_GNUTLS
            if (client->secure)
            {
                client->state = SSL_HANDSHAKING;
                gnutls_init(&client->tls_session, GNUTLS_CLIENT);
                gnutls_server_name_set(client->tls_session, GNUTLS_NAME_DNS, 
                    client->ssl_host, strlen(client->ssl_host));
                gnutls_priority_set(client->tls_session, 
                    *client->tls_priority_cache);
                gnutls_credentials_set(client->tls_session, 
                    GNUTLS_CRD_CERTIFICATE, *client->ssl_cred);
                gnutls_transport_set_ptr(client->tls_session, 
                    (gnutls_transport_ptr_t)(int_to_ptr) client->fd);
                gnutls_session_set_ptr(client->tls_session, 
                    (void*) client->ssl_host);
                gnutls_transport_set_push_function(client->tls_session, 
                    _ssl_push);
                gnutls_transport_set_pull_function(client->tls_session, 
                    _ssl_pull);
                ev_io_start(client->loop, &client->write_cb);
            }
            else
#endif
            {
                client->state = READY;
                if (client->client_cb)
                {
                    (*(client->client_cb))(client, client->cb_data);
                }
            }
        }
        else 
        {
            strerror_r(errno, client->buffer, sizeof(client->buffer));
#if DEBUG
            fprintf(stderr, "Error in connecting: %s\n", client->buffer);
#endif
            client->state = CLOSED;
            if (client->client_cb)
            {
                (*(client->client_cb))(client, client->cb_data);
                client->client_cb = NULL;
            }
        }
        return;
    }

#if DEBUG
    printf("Connected.\n");
#endif

#if HAVE_GNUTLS
    if (client->secure)
    {
        client->state = SSL_HANDSHAKING;
        gnutls_init(&client->tls_session, GNUTLS_CLIENT);
        gnutls_server_name_set(client->tls_session, GNUTLS_NAME_DNS, 
            client->ssl_host, strlen(client->ssl_host));
        gnutls_priority_set(client->tls_session, *client->tls_priority_cache);
        gnutls_credentials_set(client->tls_session, GNUTLS_CRD_CERTIFICATE, 
            *client->ssl_cred);
        gnutls_transport_set_ptr(client->tls_session, 
            (gnutls_transport_ptr_t)(int_to_ptr) client->fd);
        gnutls_session_set_ptr(client->tls_session, (void*) client->ssl_host);
        gnutls_transport_set_push_function(client->tls_session, _ssl_push);
        gnutls_transport_set_pull_function(client->tls_session, _ssl_pull);
        ev_io_start(client->loop, &client->write_cb);
    }
    else
#endif
    {
        client->state = READY;
        if (client->client_cb)
        {
            (*(client->client_cb))(client, client->cb_data);
        }
    }
}

static void 
_on_read(struct ev_loop* loop, ev_io* w, int revents)
{
    hbclient* client = (hbclient *) (((char *) w) -
        offsetof(hbclient, read_cb));
    ev_io_stop(client->loop, &client->read_cb);

#if HAVE_GNUTLS
    if (client->secure && client->state == SSL_HANDSHAKING)
    {
        _ssl_handshake(client);
        return;
    }
#endif

    int bytes_read = 0, room_avail, eoh = 0;
    if (client->state == READING_HEADERS)
    {
#if DEBUG
    printf("Reading response headers\n");
#endif
        do
        {
            room_avail = 1024 - client->rpos;
            if (! room_avail) // response too long
            {
#if DEBUG
    fprintf(stderr, "Response too long\n");
#endif
                client->state = READY;
                _rearm_client(client);
                if (client->success_cb) 
                {
                    (*client->success_cb)(client, client->cb_data);
                }
                return;
            }

            bytes_read = _conn_read(client, client->buffer + client->rpos,
                              room_avail);
            if (bytes_read < 0)
            {
#if HAVE_GNUTLS
                if (client->secure)
                {
                    if (bytes_read != GNUTLS_E_AGAIN
                        && bytes_read != GNUTLS_E_INTERRUPTED)
                    {
                        strcpy(client->buffer, gnutls_strerror(bytes_read));
#if DEBUG
    fprintf(stderr, "Error in reading headers. %s\n", client->buffer);
#endif
                        hbclient_close(client, 0);
                        client->state = CLOSED;
                        if (client->err_cb)
                            (*client->err_cb)(client, client->cb_data);
                        return;
                    }
                }
                else
#endif
                {
                    if (errno != EAGAIN)
                    {
                        strerror_r(errno, client->buffer, 
                            sizeof(client->buffer));
#if DEBUG
    fprintf(stderr, "Error in reading headers. %s\n", client->buffer);
#endif
                        hbclient_close(client, 0);
                        client->state = CLOSED;
                        if (client->err_cb)
                            (*client->err_cb)(client, client->cb_data);
                        return;
                    }
                }
                ev_io_start(client->loop, &client->read_cb);
            }
            else
            {
                client->rpos += bytes_read;
                eoh = 0;
                if ((eoh = _find_eoh(client->buffer, client->rpos)))
                {
#if DEBUG
    printf("Headers read\n");
#endif
                    client->response_len = eoh;
                    int herr = _parse_headers(client, eoh);
                    if (herr < 1)
                    {
                        hbclient_close(client, 0);
                        client->state = CLOSED;
                        if (client->err_cb)
                            (*client->err_cb)(client, client->cb_data);
                        return;
                    }
                    client->state = READING_BODY;
#if DEBUG
        printf("Reading response body.\n");
#endif
                    break;
                }
                else
                    ev_io_start(client->loop, &client->read_cb);
            }
        } while(bytes_read > 0);
    }

    if (client->state == READING_BODY)
    {
        do
        {   
            if (client->chunked)
            {
                hbcd_decode(&client->cdstate, client->buffer + eoh, 
                    bytes_read - eoh);
            }
            eoh = 0;
            bytes_read = _conn_read(client, client->buffer, 1024);
            if (bytes_read < 0)
            {
#if HAVE_GNUTLS
                if (client->secure)
                {
                    if (bytes_read == GNUTLS_E_AGAIN 
                        || bytes_read == GNUTLS_E_INTERRUPTED) 
                        break;
                    strcpy(client->buffer, gnutls_strerror(bytes_read));
                }
                else
#endif
                {
                    if (errno == EAGAIN) break; // no data to read. would block
                    // some other error occurred
                    strerror_r(errno, client->buffer, sizeof(client->buffer));
                }
                hbclient_close(client, 0);
                client->state = CLOSED;
                if (client->err_cb)
                    (*client->err_cb)(client, client->cb_data);
                return;
            }
            client->rpos += bytes_read;
        } while (bytes_read > 0);

        if (! client->chunked && client->response_len <= client->rpos)
        {
#if DEBUG
        printf("Response body read completely.\n");
#endif
            _rearm_client(client);
            if (client->success_cb)
            {
                (*client->success_cb)(client, client->cb_data);
            }
        }
        else if (client->chunked && client->cdstate.state == HBCD_FINISHED)
        {
#if DEBUG
        printf("Response body read completely.\n");
#endif
            _rearm_client(client);
            if (client->success_cb)
            {
                (*client->success_cb)(client, client->cb_data);
            }
        }
        else
            ev_io_start(client->loop, &client->read_cb);
    }
}

static void 
_on_write(struct ev_loop* loop, ev_io* w, int revents)
{
    hbclient* client = (hbclient *) (((char *) w) -
        offsetof(hbclient, write_cb));
    ev_io_stop(client->loop, &client->write_cb);

#if HAVE_GNUTLS
    if (client->secure && client->state == SSL_HANDSHAKING)
    {
        _ssl_handshake(client);
        return;
    }
#endif

    int len = hbrequest_getreqlen(client->request);
#if DEBUG
    client->request->req_data[len] = '\0';
    printf("Sending Request:\n%s\n", client->request->req_data);
#endif
    do
    {
        int bytes_sent = _conn_write(client, 
                               client->request->req_data + client->wpos,
                               len - client->wpos);
        if (bytes_sent < 0)
        {
#if HAVE_GNUTLS
            if (client->secure)
            {
                if (bytes_sent == GNUTLS_E_INTERRUPTED
                    || bytes_sent == GNUTLS_E_AGAIN)
                {
                    ev_io_start(client->loop, &client->write_cb);
                    return;
                }
                strcpy(client->buffer, gnutls_strerror(bytes_sent));
            }
            else
#endif
            {
                if (errno == EAGAIN)
                {
                    ev_io_start(client->loop, &client->write_cb);
                    return;
                }
                strerror_r(errno, client->buffer, sizeof(client->buffer));
            }
#if DEBUG
    fprintf(stderr, "Error in sending request: %s\n", client->buffer);
#endif
            hbclient_close(client, 0);
            client->state = CLOSED;
            if (client->err_cb)
                (*client->err_cb)(client, client->cb_data);
            return;
        }
        client->wpos += bytes_sent;
    } while (client->wpos < len);

#if DEBUG
    printf("Request sent.\n");
#endif
    client->state = READING_HEADERS;
    ev_io_start(client->loop, &client->read_cb);
}
