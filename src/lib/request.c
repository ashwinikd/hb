#include "request.h"

static int valid_http_token_char(char c);

hbreqheader*
hbreqheader_new(char* headline)
{
    hbreqheader* header = malloc(sizeof(hbreqheader));
    header->key = NULL;
    header->value = NULL;
    char* tmp = strdup(headline);

    int field_done = 0;
    int value_started = 0;
    int value_start_i = -1;
    int value_end_i = -1;
    int cr_found = 0;
    int lf_found = 0;
    for (int i = 0; i < strlen(headline); i++)
    {
        char c = headline[i];
        if (field_done)
        {
            if (! value_started && ( c == ' ' || c == '\t')) // leading OWS
            {
                value_start_i = i;
                continue;
            }

            value_started = 1;
            if (cr_found)
            {
                if (c != '\n') // \r without following \n
                {
                    free(tmp);
                    hbreqheader_free(header);
                    return NULL;
                }
                else
                {
                    lf_found = 1;
                    cr_found = 0;
                    value_end_i = i;
                    continue;
                }
            }
            if (lf_found)
            {
                if (c == ' ' || c == '\t')
                {
                    lf_found = 0;    
                    value_end_i = i;   
                    continue;
                } else {
                    free(tmp);
                    hbreqheader_free(header);
                    return 0;
                }
            }

            if (c > 32 && c < 127) // VCHAR
            {
                value_end_i = i;
                continue;
            }
            else if (c > 127) // obs-text
            {
                value_end_i = i;
                continue;
            }
            else if (c == ' ' || c == '\t')
                continue;
            else if (c == '\r')
            {
                value_end_i = i;
                cr_found = 1;
                continue;
            }
        }
        else
        {
            if (c == ':')
            {
                field_done = 1;
                if (!i)
                {
                    free(tmp);
                    hbreqheader_free(header);
                    return NULL;
                }
                else
                {
                    value_start_i = i;
                    tmp[i] = '\0';
                    header->key = strdup(tmp);
                    continue;
                }
            }

            if (! valid_http_token_char(c))
            {
                free(tmp);
                hbreqheader_free(header);
                return NULL;
            }
        }
    }

    if (! value_started || cr_found || lf_found)
    {
        free(tmp);
        hbreqheader_free(header);
        return NULL;
    }

    tmp[value_end_i + 1] = '\0';
    header->value = strdup(tmp + value_start_i + 1);
    free(tmp);
    return header;
}

void
hbreqheader_free(hbreqheader* header)
{
    if (! header) return;
    if (header->key)
    {
        free(header->key);
        header->key = NULL;
    }
    if (header->value)
    {
        free(header->value);
        header->value = NULL;
    }
    free(header);
}

hbrequest*
hbrequest_new()
{
    hbrequest* request = malloc(sizeof(hbrequest));
    request->method = HTTP_METHOD_GET;
    request->path = strdup("/");
    request->version = HTTP_VERSION_1_1;
    request->n_headers = 0;
    request->headers = NULL;
    request->content_length = 0;
    request->content = NULL;
    request->req_datalen = 0;
    request->req_data = NULL;
    return request;
}

int
hbrequest_addhead(hbrequest* request, char* headline)
{
    hbreqheader* new_header = hbreqheader_new(headline);
    if (! new_header) return 0;
    for (int i = 0; i < request->n_headers; i++)
    {
        hbreqheader* header = request->headers[i];
        if (! strcasecmp(header->key, new_header->key))
        {
            request->headers[i] = new_header;
            hbreqheader_free(header);
            return 1;
        }
    }

    // header does not exist
    int n_headers = request->n_headers + 1;
    if (! request->headers)
    {
        request->headers = malloc(n_headers * sizeof(hbreqheader*));
        if (! request->headers)
        {
            hbreqheader_free(new_header);
            return 0;
        }
    }
    else
    {
        hbreqheader** new_headers = realloc(request->headers,
                            n_headers * sizeof(hbreqheader*));
        if (! request->headers)
        {
            hbreqheader_free(new_header);
            return 0;
        }
        request->headers = new_headers;
    }
    request->headers[n_headers - 1] = new_header;
    request->n_headers = n_headers;

    return 2;
}

int
hbrequest_getreqlen(hbrequest* request)
{
    if (request->req_datalen > 0) return request->req_datalen;

    char data[1024];
    data[0] = '\0';
    int wpos = 0;

    // requset line
    wpos += sprintf(data + wpos, 
            "%s %s %s\r\n",
            request->method,
            request->path,
            request->version
        );

    // headers
    for(int i = 0; i < request->n_headers; i++) {
        hbreqheader* header = request->headers[i];
        wpos += sprintf(data + wpos, "%s: %s\r\n", 
                    header->key, header->value);
    }

    // empty line
    wpos += sprintf(data + wpos, "\r\n");

    // request body
    if (request->content_length > 0)
    {
        wpos += snprintf(data + wpos, request->content_length, "%s",
            request->content);
    }

    request->req_data = malloc(wpos * sizeof(char));
    if (! request->req_data) return -1;

    strncpy(request->req_data, data, wpos);
    request->req_datalen = wpos;

    return request->req_datalen;
}

void
hbrequest_print(hbrequest* request)
{
    char headers[1024];
    headers[0] = '\0';
    int wpos = 0;
    for(int i = 0; i < request->n_headers; i++) {
        hbreqheader* header = request->headers[i];
        wpos += sprintf(headers + wpos, "%s: %s\n", 
                    header->key, header->value);
    }
    printf("==== Request Start ====\n"
        "%s %s %s\n"
        "%s\n"
        "==== Request End ====\n",
        request->method,
        request->path,
        request->version,
        headers
    );
}

void
hbrequest_free(hbrequest* request)
{
    if (! request) return;
    if (request->path)
    {
        free(request->path);
        request->path = NULL;
    }
    if (request->headers)
    {
        for (int i = 0; i < request->n_headers; i++)
        {
            if (request->headers[i])
            {
                hbreqheader_free(request->headers[i]);
                request->headers[i] = NULL;
            }
        }
        free(request->headers);
        request->headers = NULL;
    }
    if (request->content)
    {
        free(request->content);
        request->content = NULL;
    }
    if (request->req_data)
    {
        free(request->req_data);
        request->req_data = NULL;
    }
    free(request);
}

/**
 * see https://tools.ietf.org/html/rfc1945#section-2.2
 */
static int
valid_http_token_char(char c)
{
    if (c > 126 || c < 32) // DEL or CTL
        return 0;

    if (c == '(' || c == ')' || c == '<' || c == '>' || c == '@' || c == ',' 
        || c == ';' || c == ':' || c == '\\' || c == '\'' || c == '/' 
        || c == '[' || c == ']' || c == '?' || c == '=' || c == '{' || c == '}' 
        || c == ' ' || c == '\t') // tspecials
        return 0;

    return 1;
}
