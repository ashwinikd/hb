#ifndef _REQUEST_H_
#define _REQUEST_H_

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

// HTTP Methods
#define HTTP_METHOD_GET       "GET"
#define HTTP_METHOD_POST      "POST"
#define HTTP_METHOD_PUT       "PUT"
#define HTTP_METHOD_DELETE    "DELETE"
#define HTTP_METHOD_HEAD      "HEAD"
#define HTTP_METHOD_OPTIONS   "OPTIONS"
#define HTTP_METHOD_TRACE     "TRACE"

// HTTP versions
#define HTTP_VERSION_1_0  "HTTP/1.0"
#define HTTP_VERSION_1_1  "HTTP/1.1"

// HTTP Header
typedef struct hbreqheader
{
    char* key;
    char* value;
} hbreqheader;

hbreqheader* hbreqheader_new(char* headline);
void hbreqheader_free(hbreqheader* header);

// HTTP Request
typedef struct hbrequest
{
    char* method;
    char* path;
    char* version;
    int n_headers;
    hbreqheader** headers;
    int content_length;
    char* content;
    int req_datalen;
    char* req_data;
} hbrequest;

hbrequest* hbrequest_new();
int hbrequest_addhead(hbrequest* request, char* headline);
int hbrequest_getreqlen(hbrequest* request);
void hbrequest_print(hbrequest* request);
void hbrequest_free(hbrequest* request);

#endif
