#ifndef _CLIENT_H_
#define _CLIENT_H_

#include "../../config.h"
#include <sys/socket.h>
#include <netdb.h>
#include <stdlib.h>
#include <ev.h>
#include "request.h"
#include <fcntl.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <stddef.h>
#include <errno.h>
#include <stdio.h>
#include <unistd.h>

#if HAVE_GNUTLS
#include <gnutls/gnutls.h>
#include <gnutls/x509.h>

#if (__SIZEOF_POINTER__==8)
typedef uint64_t int_to_ptr;
#else
typedef uint32_t int_to_ptr;
#endif

#endif

#define HBCLIENT_ERR_OK          0 /* All OK */  
#define HBCLIENT_ERR_INVALID     1 /* Client is null */
#define HBCLIENT_ERR_STATE       2 /* Client state is not valid for operation */
#define HBCLIENT_ERR_CONN        3 /* Connection error */

enum hbcdstate_code {
    HBCD_STOPPED,
    HBCD_IDLE,
    HBCD_READING_LEN,
    HBCD_READING_BODY,
    HBCD_READING_TRLR,
    HBCD_FINISHED
};

typedef struct hbcdstate {
    int state;
    int n_chunk;
    int chunk_size;
    int chunk_rpos;
    int cr_found;
    int crlf_cnt;
    int len;
    char buf[16];
} hbcdstate;

void hbcdstate_init(hbcdstate* state);
void hbcd_decode(hbcdstate* state, char* buf, int len);

enum hbclient_state {
    CLOSED, 
    CONNECTING,
    SSL_HANDSHAKING,
    SSL_HANDSHAKE_FAILED,
    READY,
    WRITING,
    READING_HEADERS,
    READING_BODY
};

typedef struct hbclient {
    int state;
    struct addrinfo* addr;
    int fd;
    
#if HAVE_GNUTLS
    int secure;
    gnutls_session_t tls_session;
    char* ssl_host;
    gnutls_priority_t* tls_priority_cache;
    gnutls_certificate_credentials_t* ssl_cred;
#endif

    struct ev_loop* loop;
    struct ev_io read_cb;
    struct ev_io write_cb;
    struct ev_io conn_cb;

    void (*client_cb)(struct hbclient* client, void* cb_data);
    void (*success_cb)(struct hbclient* client, void* cb_data);
    void (*err_cb)(struct hbclient* client, void* cb_data);
    void* cb_data;

    hbrequest* request;
    int wpos;
    char buffer[1024];
    int rpos;

    int chunked;
    hbcdstate cdstate;
    int keep_alive;
    int response_keep_alive;
    int response_len;
} hbclient;

typedef void (*hbclient_cb)(hbclient* client, void* cb_data);

hbclient* hbclient_new(struct ev_loop* loop, struct addrinfo* addr);
void hbclient_free(hbclient* client);
int hbclient_connect(hbclient* client, hbclient_cb cb, void* cb_data);
int hbclient_execute(hbclient* client, hbrequest* request,
                     hbclient_cb success_cb, hbclient_cb error_cb, void* data);
void hbclient_close(hbclient* client, int good);

#endif
