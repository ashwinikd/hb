#ifndef _WORKER_H_
#define _WORKER_H_

#include "../../config.h"
#include "request.h"
#include "client.h"
#include <stdio.h>

/*
    Defining flags to be passed to ev_loop_new
 */
#if defined(__APPLE__) || defined(__BSD__)
    #if HAVE_KQUEUE
        #define EVLOOP_FLAG EVBACKEND_KQUEUE
    #elif HAVE_POLL
        #define EVLOOP_FLAG EVBACKEND_POLL
    #else
        #define EVLOOP_FLAG 0
    #endif
#elif defined(__linux__)
    #if HAVE_EPOLL
        #define EVLOOP_FLAG EVBACKEND_EPOLL
    #elif HAVE_POLL
        #define EVLOOP_FLAG EVBACKEND_POLL
    #else
        #define EVLOOP_FLAG 0
    #endif
#else
    #if HAVE_POLL
        #define EVLOOP_FLAG EVBACKEND_POLL
    #else
        #define EVLOOP_FLAG 0
    #endif
#endif

void hbwork();

#endif